﻿using CefSharp;
using CefSharp.WinForms;
using System;
using System.Diagnostics;
using System.IO;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace kilit
{
    public partial class Form1 : Form
    {
        public ChromiumWebBrowser chromeBrowser;
        public void InitializeChromium()
        {
            CefSettings settings = new CefSettings();
            Cef.Initialize(settings);
            chromeBrowser = new ChromiumWebBrowser("https://giris.eba.gov.tr/EBA_GIRIS/teacher.jsp");
            this.Controls.Add(chromeBrowser);
            chromeBrowser.Dock = DockStyle.Fill;
            chromeBrowser.AddressChanged += ChromeBrowser_AddressChanged;
        }

        private void Tm_Tick(object sender, EventArgs e)
        {
            SayfadanDegerKontrolEt();
            if (EvaluateJavaScriptResult == "EBA Kod Oluştur")
            {
                closeVerify = true;
                Application.Exit();
            }
        }

        Timer tm;
        private void ChromeBrowser_AddressChanged(object sender, AddressChangedEventArgs e)
        {
            if (e.Address.Contains("https://www.eba.gov.tr/ders"))
            {
                chromeBrowser.Load("https://www.eba.gov.tr/");
            }
            else if (e.Address == "http://www.eba.gov.tr/" || e.Address == "https://www.eba.gov.tr/")
            {
                SayfadanDegerKontrolEt();
                tm = new Timer();
                tm.Enabled = true;
                tm.Interval = 2000;
                tm.Tick += Tm_Tick;
            }
            else
            {
                //MessageBox.Show("Hatalı Giriş.", "Hata", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        string EvaluateJavaScriptResult;
        private void SayfadanDegerKontrolEt()
        {
            var frame = chromeBrowser.GetMainFrame();
            var task = frame.EvaluateScriptAsync("(function() { return document.getElementsByClassName('drop')[3].innerText; })();", null);

            task.ContinueWith(t =>
            {
                if (!t.IsFaulted)
                {
                    var response = t.Result;
                    EvaluateJavaScriptResult = response.Success ? (response.Result.ToString() ?? "null") : response.Message;
                }
            }, TaskScheduler.Default);
        }

        public Form1()
        {
            InitializeComponent();
            try
            {
                InitializeChromium();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Beklenmeyen hata oluştu. " + ex.Message, "Kilit İşlemi", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
        private void btnBilKapat_Click(object sender, System.EventArgs e)
        {
            DialogResult cevap = MessageBox.Show("Kapatma işlemini onaylıyor musunuz?", "Kapatma İşlemi", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
            if (cevap == DialogResult.Yes)
            {
                Process.Start("ShutDown", "/s -f -t 1");
            }
        }
        private void KlavyeAc()
        {
            var path64 = Path.Combine(Directory.GetDirectories(Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.Windows), "winsxs"), "amd64_microsoft-windows-osk_*")[0], "osk.exe");
            var path32 = @"C:\windows\system32\osk.exe";
            var path = (Environment.Is64BitOperatingSystem) ? path64 : path32;
            Process.Start(path);
        }
        private void bntKlavyeAc_Click(object sender, System.EventArgs e)
        {
            KlavyeAc();
        }        
        private bool closeVerify = false;
        private void Form1_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (closeVerify)
            {
                e.Cancel = false;
            }
            else
            {
                e.Cancel = true;
            }
        }
    }
}
