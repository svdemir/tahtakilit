﻿namespace kilit
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnBilKapat = new System.Windows.Forms.Button();
            this.bntKlavyeAc = new System.Windows.Forms.Button();
            this.lblVersiyon = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // btnBilKapat
            // 
            this.btnBilKapat.BackColor = System.Drawing.Color.SlateGray;
            this.btnBilKapat.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.btnBilKapat.Font = new System.Drawing.Font("Microsoft Sans Serif", 30.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.btnBilKapat.Location = new System.Drawing.Point(0, 392);
            this.btnBilKapat.Name = "btnBilKapat";
            this.btnBilKapat.Size = new System.Drawing.Size(822, 97);
            this.btnBilKapat.TabIndex = 0;
            this.btnBilKapat.Text = "Tahtayı Kapat";
            this.btnBilKapat.UseVisualStyleBackColor = false;
            this.btnBilKapat.Click += new System.EventHandler(this.btnBilKapat_Click);
            // 
            // bntKlavyeAc
            // 
            this.bntKlavyeAc.BackColor = System.Drawing.Color.LightSteelBlue;
            this.bntKlavyeAc.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.bntKlavyeAc.Font = new System.Drawing.Font("Microsoft Sans Serif", 30.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.bntKlavyeAc.Location = new System.Drawing.Point(0, 295);
            this.bntKlavyeAc.Name = "bntKlavyeAc";
            this.bntKlavyeAc.Size = new System.Drawing.Size(822, 97);
            this.bntKlavyeAc.TabIndex = 1;
            this.bntKlavyeAc.Text = "Ekran Klavyesi";
            this.bntKlavyeAc.UseVisualStyleBackColor = false;
            this.bntKlavyeAc.Click += new System.EventHandler(this.bntKlavyeAc_Click);
            // 
            // lblVersiyon
            // 
            this.lblVersiyon.AutoSize = true;
            this.lblVersiyon.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.lblVersiyon.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblVersiyon.Location = new System.Drawing.Point(0, 278);
            this.lblVersiyon.Name = "lblVersiyon";
            this.lblVersiyon.Size = new System.Drawing.Size(33, 17);
            this.lblVersiyon.TabIndex = 2;
            this.lblVersiyon.Text = "v2.0";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(822, 489);
            this.ControlBox = false;
            this.Controls.Add(this.lblVersiyon);
            this.Controls.Add(this.bntKlavyeAc);
            this.Controls.Add(this.btnBilKapat);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.KeyPreview = true;
            this.Name = "Form1";
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.WindowsDefaultBounds;
            this.TopMost = true;
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.Form1_FormClosing);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnBilKapat;
        private System.Windows.Forms.Button bntKlavyeAc;
        private System.Windows.Forms.Label lblVersiyon;
    }
}

